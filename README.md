# laravel multi-auth

A simple and complete laravel application allows multiple type of user types(admin/users)

Getting started
Installation
Please check the official laravel installation guide for server requirements before you start. Official Documentation

#Clone the repository

git@gitlab.com:nirodhas/laravel-multi-auth.git
#Switch to the repo folder

cd laravel-multi-auth
#Install all the dependencies using composer

composer install
#Copy the example env file and make the required configuration changes in the .env file

cp .env.example .env
#Generate a new application key

php artisan key:generate
#Migrate database 


php artisan migrate
#Start the local development server

php artisan serve
#You can now access the server at http://localhost:8000

