<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AdminLoginController extends Controller
{
    //setting middleware

    public function __construct(){
        $this->middleware('guest:admin');
    }

    public function showLoginForm(){

        return view('auth.admin-login');
    }

    public function login(Request $request){
        //validate the form data
        $this->validate($request , [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);
        //attempt to user in 
        if(Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)){
            //if success -> intended locaation
            return redirect()->intended(route('admin.dashboard'));
        }
        
        return redirect()-> back()->withInput($request->only('email', 'remembr'));

        //unsuccess -> login
    }
}
